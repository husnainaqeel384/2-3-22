const redis = require('../Redis/redis');
const axios = require('axios')

const postdata = {
    async addpost(req, res) {
        const { key, value } = req.body;
        const response = await redis.set(key, value)
        res.json(response);
    },
    getpost(req, res) {
        const { key } = posts;
        redis.get(key, (err, result) => {
            if (err) res.json(err);
            res.json(result)
        });

    },
    async getpost(req, res) {
        const { id } = req.params;
        const cached = await redis.get(`post-${id}`);
        if (cached)
            res.json(JSON.parse(cached));
        const response = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`);
        redis.set(`post-${id}`, JSON.stringify(response.data), "EX", 10);
        return res.json(response.data);
    }
}
module.exports = postdata; 