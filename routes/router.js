const express = require('express');
const router = express.Router();
const controller = require('../controller/controller');
router.route('/').post(controller.addpost);

router.route('/').get(controller.getpost);

router.route('/posts/:id').get(controller.getpost);
module.exports=router;