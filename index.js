const express = require('express');
const app = express();
const bodyparser = require('body-parser');

const dotenv = require('dotenv');

dotenv.config({ path: './.env' });
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());

const router = require('./routes/router')

app.use('/', router);

app.listen(process.env.PORT, () => {
    console.log(`listening on port ${process.env.PORT}`);
})
